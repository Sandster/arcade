import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';

class DropdownInput extends StatelessWidget {
  //final List<Map<String, dynamic>> inputData;
  final List inputData;
  final String placeholderText;

  //final Map<String, dynamic>? value;
  final Object? value;
  final ValueSetter<dynamic>? onChange;
  final Icon? icon;
  final bool isDisabled;

  const DropdownInput(
      {Key? key,
      required this.inputData,
      required this.placeholderText,
      required this.value,
      this.icon,
      this.isDisabled = false,
      required this.onChange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
          color: Colors.grey[700], borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.only(left: 10.0, right: 10.0),
        child: DropdownButton(
          isExpanded: false,
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          hint: Container(
            width: 150, //and here
            child: Text(
              placeholderText,
              style: TextStyle(color: primaryColor),
              textAlign: TextAlign.start,
            ),
          ),
          value: value,
          underline: Container(),
          icon: Padding(
            padding: EdgeInsets.only(left: 10),
            child: icon,
          ),
          iconEnabledColor: primaryColor,
          disabledHint: Text(
            'Loading',
            style: TextStyle(color: primaryColor),
            textAlign: TextAlign.start,
          ),
          elevation: 16,
          style: const TextStyle(color: primaryColor),
          onChanged: isDisabled ? null : onChange,
          items: inputData.map<DropdownMenuItem>((dynamic value) {
            return DropdownMenuItem(
              value: value,
              child: Text(value.name),
            );
          }).toList(),
          // items: inputData.map<DropdownMenuItem<Map<String, dynamic>>>(
          //     (Map<String, dynamic> value) {
          //   return DropdownMenuItem<Map<String, dynamic>>(
          //     value: value,
          //     child: Text(value['name']),
          //   );
          // }).toList(),
        ),
      ),
    );
  }
}
