import 'package:arcade/screens/deals/deal_loading.dart';
import 'package:arcade/screens/giveaways/giveaway_loading.dart';
import 'package:flutter/material.dart';

enum CardType {
  giveaway,
  deal,
  browse,
}

class ShimmerPlaceholder extends StatelessWidget {
  final CardType type;

  const ShimmerPlaceholder({Key? key, required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: 4,
        itemBuilder: (context, index) {
          if (type == CardType.giveaway || type == CardType.browse) {
            return const GiveawayLoading();
          } else if (type == CardType.deal) {
            return DealLoading();
          } else {
            return Container();
          }
        });
  }
}
