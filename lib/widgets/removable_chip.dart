import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';

class RemovableChip extends StatelessWidget {
  final String label;
  final VoidCallback onDelete;

  const RemovableChip({Key? key, required this.label, required this.onDelete})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InputChip(
      label: Text(label),
      backgroundColor: accentColor,
      labelStyle: TextStyle(fontWeight: FontWeight.bold, color: primaryColor),
      onDeleted: onDelete,
    );
  }
}
