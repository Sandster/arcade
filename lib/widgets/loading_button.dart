import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class LoadingButton extends StatelessWidget {
  final RoundedLoadingButtonController controller;
  final String text;
  final VoidCallback onPressed;
  final double width;

  const LoadingButton(
      {Key? key,
      required this.controller,
      required this.text,
      this.width = 300,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RoundedLoadingButton(
      width: width,
      color: accentColor,
      child: Text(text, style: TextStyle(color: primaryColor)),
      controller: controller,
      onPressed: onPressed,
    );
  }
}
