import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';

class TextFieldInput extends StatelessWidget {
  final TextEditingController? controller;
  final TextInputType inputType;
  final TextInputAction inputAction;
  final IconData icon;
  final String hint;
  final String label;
  final String? errorText;
  final bool isPassword;
  final StringValidationCallback validationCallback;
  final ValueSetter<String>? onChange;
  final ValueSetter<String>? onSubmit;

  const TextFieldInput(
      {Key? key,
      this.controller,
      required this.inputType,
      required this.icon,
      required this.hint,
      required this.label,
      required this.validationCallback,
      this.errorText,
      this.onChange,
      this.onSubmit,
      this.isPassword = false,
      this.inputAction = TextInputAction.done})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: onChange,
      controller: controller,
      autofocus: false,
      keyboardType: inputType,
      obscureText: isPassword,
      textInputAction: inputAction,
      onFieldSubmitted: onSubmit,
      decoration: InputDecoration(
          prefixIcon: Icon(
            icon,
            color: primaryColor,
          ),
          errorText: errorText,
          contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
          hintText: hint,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
          labelText: label),
      validator: validationCallback,
    );
  }
}
