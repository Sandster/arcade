import 'dart:convert';

import 'package:arcade/models/preferences.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/request_type.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Task> getAllPreferences({required BuildContext context}) async {
  var provider = Provider.of<PreferencesModel>(context, listen: false);

  Task genreTask =
      await HttpClient(path: 'genre', isAuthenticated: true).sendRequest();

  Task platformTask =
      await HttpClient(path: 'platform', isAuthenticated: true).sendRequest();

  if (genreTask.success && platformTask.success) {
    //update data
    provider.fromJson(false, genreTask.body);
    provider.fromJson(true, platformTask.body);
  }

  return !genreTask.success ? genreTask : platformTask;
}

Future<Task> updateAllPreferences({required BuildContext context}) async {
  var provider = Provider.of<PreferencesModel>(context, listen: false);

  String genres = provider.toJson(true);
  String platforms = provider.toJson(false);

  Task genreTask = await HttpClient(
          path: 'genre/update',
          isAuthenticated: true,
          body: genres,
          type: RequestType.PATCH)
      .sendRequest();

  Task platformTask = await HttpClient(
          path: 'platform/update',
          isAuthenticated: true,
          body: platforms,
          type: RequestType.PATCH)
      .sendRequest();

  if (genreTask.success && platformTask.success) {
    await _saveToStorage();
  }

  return !genreTask.success ? genreTask : platformTask;
}

Future<Task> _saveToStorage() async {
  Task? task =
      await HttpClient(path: 'auth/get', isAuthenticated: true).sendRequest();

  if (task.success) {
    Map<String, dynamic> body = task.body;
    List<dynamic> members = body['payload'];
    Map<String, dynamic> user = members.elementAt(0);
    String userData = jsonEncode(user);

    //add to local storage
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('user', userData);
  }

  return task;
}
