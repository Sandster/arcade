import 'dart:convert';

import 'package:arcade/models/browse.dart';
import 'package:arcade/utils/helpers.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Task> getAllGames({required BuildContext context}) async {
  var provider = Provider.of<BrowseModel>(context, listen: false);

  String query = '?resultCount=4';
  String genres = await _retrieveUserGenres();

  genres.isNotEmpty ? query += '&genres=$genres' : null;

  Task allGamesTask = await HttpClient(
          path: 'browse/genre', query: query, isAuthenticated: true)
      .sendRequest();

  allGamesTask.success ? provider.fromJson(allGamesTask.body) : null;

  return allGamesTask;
}

Future<Task> searchGame({required BuildContext context, required String searchCriteria})async{
  var provider = Provider.of<BrowseModel>(context, listen: false);
  provider.removeAll();
  String query = '?resultCount=4&search=$searchCriteria';
  Task searchResult =
  await HttpClient(path: 'browse', query: query, isAuthenticated: true)
      .sendRequest();

  if(searchResult.success){
    provider.removeAll();
    provider.fromJson(searchResult.body);
  }
  return searchResult;
}

Future<String> _retrieveUserGenres() async {
  final prefs = await SharedPreferences.getInstance();
  String user = prefs.getString('user') ?? '';

  Map<String, dynamic> decodedUser = jsonDecode(user);

  List<Map<String, dynamic>> castData =
      payloadCast(data: decodedUser['preferredGenres']);

  String genres = '';

  castData.asMap().forEach((key, value) {
    key == 0
        ? genres += value['name'].toString().toLowerCase()
        : genres += ',${value['name'].toString().toLowerCase()}';
  });

  return genres;
}
