import 'dart:convert';

import 'package:arcade/models/profile.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

getUserData({required BuildContext context}) async {
  var provider = Provider.of<ProfileModel>(context, listen: false);

  final prefs = await SharedPreferences.getInstance();
  String user = prefs.getString('user') ?? '';
  Map<String, dynamic> userData = jsonDecode(user);

  provider.username = userData['username'];
  provider.email = userData['email'];
}

getUserSettings({required BuildContext context}) async {
  final prefs = await SharedPreferences.getInstance();
  bool isEnabled = prefs.getBool('defaultBrowserEnabled') ?? false;
  Provider.of<ProfileModel>(context, listen: false).isDefaultBrowserActivated =
      isEnabled;
}

updateUserSettings(bool isEnabled) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setBool('defaultBrowserEnabled', isEnabled);
}

Future<Task> getUserWishlist({required BuildContext context}) async {
  var provider = Provider.of<ProfileModel>(context, listen: false);
  Task wishlistTask =
      await HttpClient(path: 'wishlist', isAuthenticated: true).sendRequest();

  provider.wishlist.isNotEmpty ? provider.removeAll() : null;

  wishlistTask.success ? provider.fromJson(wishlistTask.body) : null;

  return wishlistTask;
}
