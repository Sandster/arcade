import 'package:arcade/models/giveaway.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

Future<Task> getAllGiveaways(
    {required BuildContext context,
    required String type,
    required int page,
    required bool paginate}) async {
  var provider = Provider.of<GiveawaysModel>(context, listen: false);
  String query = '?type=$type&page=$page';
  Task giveawaysTask =
      await HttpClient(path: 'giveaways', query: query, isAuthenticated: true)
          .sendRequest();

  if (giveawaysTask.success) {
    Map<String, dynamic> body = giveawaysTask.body;
    !paginate ? provider.removeAll() : null;
    provider.fromJson(body);
  }

  return giveawaysTask;
}
