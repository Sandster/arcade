import 'package:arcade/models/browse.dart';
import 'package:arcade/models/game.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/request_type.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

// Future<Task> checkWishlistAvailabilityTask(int id) async {
//   Task availability = await HttpClient(
//           path: 'wishlist/check', query: '?id=$id', isAuthenticated: true)
//       .sendRequest();
//   return availability;
// }

// Future<Task> getGameDetails(String id) async {
//   Task gameData = await HttpClient(
//           path: 'browse/get', query: '?id=$id', isAuthenticated: true)
//       .sendRequest();
//
//   return gameData;
// }

Future<Task> getGameData(
    {required BuildContext context, required int id}) async {
  var provider = Provider.of<GameModel>(context, listen: false);
  Task availability = await HttpClient(
          path: 'wishlist/check', query: '?id=$id', isAuthenticated: true)
      .sendRequest();

  Task gameData = await HttpClient(
          path: 'browse/get', query: '?id=$id', isAuthenticated: true)
      .sendRequest();

  if (availability.success && gameData.success) {
    provider.wishlistAvailability = availability.body['present'];

    provider.fromJson(gameData.body);
  }

  return !availability.success ? availability : gameData;
}

Future<Task> removeFromWishlist(
    {required BuildContext context, required int id}) async {
  var provider = Provider.of<GameModel>(context, listen: false);

  Task remove = await HttpClient(
          path: 'wishlist/remove',
          query: '?id=$id',
          type: RequestType.DELETE,
          isAuthenticated: true)
      .sendRequest();

  remove.success ? provider.wishlistAvailability = false : null;

  return remove;
}

Future<Task> addToWishlist(
    {required BuildContext context, required GameResult game}) async {
  var provider = Provider.of<GameModel>(context, listen: false);

  String body = provider.toJson(game);

  Task addTask = await HttpClient(
          path: 'wishlist/add',
          type: RequestType.POST,
          isAuthenticated: true,
          body: body)
      .sendRequest();

  addTask.success ? provider.wishlistAvailability = true : null;

  return addTask;
}
