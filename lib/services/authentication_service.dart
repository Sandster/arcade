import 'dart:convert';

import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/request_type.dart';
import 'package:arcade/utils/task.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Task> registerUser(
    {required Map<String, dynamic> registerData}) async {
  Task task = await HttpClient(
      path: 'auth/register', type: RequestType.POST, body: registerData)
      .sendRequest();

  if (task.success) {
    //persist state on device
    task = await loginUser(
        email: registerData['email'], password: registerData['password']);
  }

  return task;
}
Future<Task> loginUser(
    {required String email, required String password}) async {
  try {
    UserCredential credential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);

    final token = await credential.user!.getIdToken(true);
    await _saveUserDataTask(token: token);

    return Task(success: true, message: 'Login successful', body: {});
  } on FirebaseAuthException catch (error) {
    if (error.code == 'user-not-found' || error.code == 'wrong-password') {
      return Task(
          success: false, message: 'Invalid email or password', body: {});
    } else if (error.code == 'network-request-failed') {
      return Task(
          success: false,
          message: 'No internet connection, please try again',
          body: {});
    } else {
      return Task(success: false, message: 'Internal error', body: {});
    }
  } catch (error) {
    print(error);
    return Task(success: false, message: 'Internal error', body: {});
  }
}

Future<Task> _saveUserDataTask({required String token}) async {
  //save user token for use
  final prefs = await SharedPreferences.getInstance();
  prefs.setInt('token_iat', DateTime.now().millisecondsSinceEpoch);
  prefs.setString('token', token);

  Task task =
  await HttpClient(path: 'auth/get', isAuthenticated: true).sendRequest();

  if (task.success) {
    Map<String, dynamic> body = task.body;
    List<dynamic> members = body['payload'];
    Map<String, dynamic> user = members.elementAt(0);
    String userData = jsonEncode(user);

    //add user data to local storage
    prefs.setString('user', userData);
  }
  return task;
}

logout() async {
  await FirebaseAuth.instance.signOut();
}