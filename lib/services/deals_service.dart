import 'package:arcade/models/deal.dart';
import 'package:arcade/utils/http_client.dart';
import 'package:arcade/utils/task.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

Future<Task> getAllDeals(
    {required BuildContext context,
    required int page,
    required double minimumPrice,
    required double maximumPrice,
    required bool isSale,
    required bool paginate}) async {
  var provider = Provider.of<DealsModel>(context, listen: false);

  String query = '?pageSize=10&pageNumber=$page&sale=$isSale';

  minimumPrice > 0 ? query += '&min=$minimumPrice' : null;
  maximumPrice > 0 ? query += '&max=$maximumPrice' : null;

  Task dealsTask = await HttpClient(
          path: 'deals/onsale', query: query, isAuthenticated: true)
      .sendRequest();

  if (dealsTask.success) {
    Map<String, dynamic> body = dealsTask.body;
    !paginate ? provider.removeAll() : null;
    provider.fromJson(body);
  }
  return dealsTask;
}

Future<Task> getStoreInfo(int id) async {
  Task store = await HttpClient(
      path: 'stores/lookup', query: '?id=$id', isAuthenticated: true)
      .sendRequest();
  return store;
}
