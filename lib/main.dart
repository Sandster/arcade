import 'package:arcade/screens/dashboard_screen.dart';
import 'package:arcade/screens/login_screen.dart';
import 'package:arcade/screens/preferences_screen.dart';
import 'package:arcade/utils/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!kIsWeb) {
    await Firebase.initializeApp();
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Arcade',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark().copyWith(
          scaffoldBackgroundColor: mobileBackgroundColor,
          colorScheme: ThemeData().colorScheme.copyWith(primary: accentColor),
          errorColor: errorColor),
      home: routeMain(),
    );
  }

  Widget routeMain() {
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) {
      return LoginScreen();
    } else {
      return DashboardScreen();
    }
  }
}
