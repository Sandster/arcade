import 'package:arcade/models/base.dart';
import 'package:flutter/material.dart';

class GiveawaysModel extends ChangeNotifier with Core {
  List<Giveaway> giveaways = [];

  List<Giveaway> getGiveaways() {
    return giveaways;
  }

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      json['payload'].forEach((giveawayItem) {
        giveaways.add(new Giveaway.fromJson(giveawayItem));
      });
    }
    notifyListeners();
  }

  @override
  void removeAll() {
    giveaways.clear();
  }

  @override
  void updateData(updateData) {
    giveaways.addAll(updateData);
    notifyListeners();
  }
}

class Giveaway {
  String? image;
  String? url;
  String? title;
  String? description;
  String? publishedDate;
  bool? status;
  List<String>? platforms;

  Giveaway(
      {this.image,
      this.url,
      this.title,
      this.description,
      this.publishedDate,
      this.status,
      this.platforms});

  Giveaway.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    url = json['open_giveaway'];
    status = json['status'] == 'Active' ? true : false;
    title = json['title'];
    description = json['description'];
    publishedDate = json['published_date'];
    platforms = json['platforms'].split(', ');
  }
}
