import 'package:arcade/models/base.dart';
import 'package:arcade/models/browse.dart';
import 'package:flutter/material.dart';

class ProfileModel extends ChangeNotifier with Core {
  String _username = 'Loading';
  String _email = 'Loading';
  bool _isDefaultBrowserActivated = false;
  List<GameResult> _wishlist = [];
  bool _initialRender = true;

  bool get initialRender => _initialRender;

  set initialRender(bool value) {
    _initialRender = value;
  }

  List<GameResult> get wishlist => _wishlist;

  bool get isDefaultBrowserActivated => _isDefaultBrowserActivated;

  set isDefaultBrowserActivated(bool value) {
    _isDefaultBrowserActivated = value;
    notifyListeners();
  }

  String get username => _username;

  set username(String value) {
    _username = value;
    notifyListeners();
  }

  String get email => _email;

  set email(String value) {
    _email = value;
    notifyListeners();
  }

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      json['payload'].forEach((gameItem) {
        _wishlist.add(new GameResult.fromJson(gameItem));
      });
    }
    notifyListeners();
  }

  @override
  void removeAll() {
    _wishlist.clear();
  }

  @override
  void updateData(updateData) {}
}
