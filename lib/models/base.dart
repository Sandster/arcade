abstract class Core {
  void fromJson(Map<String, dynamic> json);

  void removeAll();

  void updateData(updateData);
}
