import 'dart:convert';

import 'package:arcade/models/base.dart';
import 'package:arcade/models/browse.dart';
import 'package:flutter/material.dart';

class GameModel extends ChangeNotifier with Core {
  bool _wishlistAvailability = false;
  GameResult? game;

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      game = new GameResult.fromJson(json['payload'][0]);
    }
    notifyListeners();
  }

  @override
  void removeAll() {
    game = null;
  }

  String toJson(GameResult game) {
    return jsonEncode({
      'name': game.name,
      'slug': game.slug,
      'releaseDate': game.releaseDate,
      'backgroundImage': game.image,
      'rating': double.parse(game.rating!),
      'metacritic': double.parse(game.metacriticRating!),
      '_id': game.id,
      'esrb': game.esrbRating,
      'genres': convertList(game.genres!),
      'platforms': convertList(game.platforms!),
    });
  }


  @override
  void updateData(updateData) {}

  bool get wishlistAvailability => _wishlistAvailability;

  set wishlistAvailability(bool value) {
    _wishlistAvailability = value;
  }
}
