import 'package:arcade/models/base.dart';
import 'package:flutter/material.dart';

class BrowseModel extends ChangeNotifier with Core {
  List<GameResult> games = [];

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      json['payload'].forEach((gameItem) {
        games.add(new GameResult.fromJson(gameItem));
      });
    }
    notifyListeners();
  }

  @override
  void removeAll() {
    games.clear();
    notifyListeners();
  }

  @override
  void updateData(updateData) {
    games.addAll(updateData);
    notifyListeners();
  }
}

class GameResult {
  int? id;
  String? image;
  String? slug;
  String? name;
  String? releaseDate;
  String? rating;
  String? description;
  String? esrbRating;
  String? metacriticRating;
  List<Platform>? platforms;
  List<Genre>? genres;
  List<Store>? stores;

  GameResult.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    image = json['backgroundImage'];
    name = json['name'];
    releaseDate = json['releaseDate'];
    rating = json['rating'].toString();
    description = json['description'] != null ? json['description'] : '';
    esrbRating = json['esrb'];
    metacriticRating = json['metacritic'].toString();
    platforms = _generatePlatforms(json['platforms']);
    genres = _generateGenres(json['genres']);
    stores = json['stores'] != null ? _generateStores(json['stores']) : [];
    slug = json['slug'];
  }
}

class Platform {
  String? id;
  String? name;
  String? slug;

  Platform(this.id, this.name, this.slug);

  Platform.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];
    slug = json['slug'];
  }
}

class Genre {
  String? id;
  String? name;
  String? slug;

  Genre(this.id, this.name, this.slug);

  Genre.fromJson(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['name'];
    slug = json['slug'];
  }
}

class Store {
  String? id;
  String? name;
  String? domain;

  Store(this.id, this.name);

  Store.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    json['domain'] != null ? domain = json['domain'] : null;
  }
}

List<Map<String, dynamic>> convertList(List listData) {
  List<Map<String, dynamic>> list = [];
  listData.forEach((element) {
    Map<String, dynamic> objectMap = {
      '_id': element.id,
      'name': element.name,
      'slug': element.slug,
    };
    list.add(objectMap);
  });
  return list;
}

List<Platform> _generatePlatforms(List<dynamic> data) {
  List<Platform> temp = [];
  data.asMap().forEach((index, value) {
    temp.add(new Platform(
        data[index]['_id'], data[index]['name'], data[index]['slug']));
  });
  return temp;
}

List<Genre> _generateGenres(List<dynamic> data) {
  List<Genre> temp = [];
  data.asMap().forEach((index, value) {
    temp.add(new Genre(
        data[index]['_id'], data[index]['name'], data[index]['slug']));
  });
  return temp;
}

List<Store> _generateStores(List<dynamic> data) {
  List<Store> temp = [];
  data.asMap().forEach((index, value) {
    temp.add(new Store(data[index]['id'].toString(), data[index]['name']));
  });
  return temp;
}
