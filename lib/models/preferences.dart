import 'dart:convert';

import 'package:arcade/models/browse.dart';
import 'package:flutter/material.dart';

class PreferencesModel extends ChangeNotifier {
  List<Genre> _allGenres = [];

  List<Platform> _allPlatforms = [];

  final List<Platform> _selectedPlatforms = [];

  final List<Genre> _selectedGenres = [];

  List<Platform> get selectedPlatforms => _selectedPlatforms;
  Genre? _currentGenre;
  Platform? _currentPlatform;

  Genre? get currentGenre => _currentGenre;

  List<Genre> get allGenres => _allGenres;

  set currentGenre(Genre? value) {
    _currentGenre = value;
    notifyListeners();
  }

  Platform? get currentPlatform => _currentPlatform;

  set currentPlatform(Platform? value) {
    _currentPlatform = value;
  }

  List<Genre> get selectedGenres => _selectedGenres;

  List<Platform> get allPlatforms => _allPlatforms;

  void addGenre(Genre genre) {
    _selectedGenres.add(genre);
    notifyListeners();
  }

  void addPlatform(Platform platform) {
    _selectedPlatforms.add(platform);
    notifyListeners();
  }

  void removeGenre(Genre genre) {
    _selectedGenres.remove(genre);
    notifyListeners();
  }

  void removePlatform(Platform platform) {
    _selectedPlatforms.remove(platform);
    notifyListeners();
  }

  void fromJson(bool isPlatform, Map<String, dynamic> json) {
    if (json['payload'] != null) {
      json['payload'].forEach((item) {
        isPlatform
            ? _allPlatforms.add(new Platform.fromJson(item))
            : _allGenres.add(new Genre.fromJson(item));
      });
    }
    notifyListeners();
  }

  String toJson(bool isGenre) {
    return isGenre
        ? jsonEncode(convertList(_selectedGenres))
        : jsonEncode(convertList(_selectedPlatforms));
  }

}
