import 'package:arcade/models/base.dart';
import 'package:flutter/material.dart';

class DealsModel extends ChangeNotifier with Core {
  List<Deal> deals = [];

  @override
  void fromJson(Map<String, dynamic> json) {
    if (json['payload'] != null) {
      json['payload'].forEach((dealItem) {
        deals.add(new Deal.fromJson(dealItem));
      });
    }
    notifyListeners();
  }

  @override
  void removeAll() {
    deals.clear();
  }

  @override
  void updateData(updateData) {
    deals.addAll(updateData);
    notifyListeners();
  }
}

class Deal {
  String? storeId;
  String? title;
  String? salePrice;
  String? normalPrice;
  String? thumbnail;
  int? lastUpdate;
  String? rating;

  Deal(
      {this.storeId,
      this.title,
      this.salePrice,
      this.normalPrice,
      this.thumbnail,
      this.lastUpdate,
      this.rating});

  Deal.fromJson(Map<String, dynamic> json) {
    storeId = json['storeID'];
    title = json['title'];
    salePrice = json['salePrice'];
    normalPrice = json['normalPrice'];
    thumbnail = json['thumb'];
    lastUpdate = json['lastChange'];
    rating = json['dealRating'];
  }
}
