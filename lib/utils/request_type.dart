enum RequestType {
  GET,
  POST,
  PATCH,
  DELETE
}