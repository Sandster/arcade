import 'package:flutter/material.dart';

const headingText1 = TextStyle(fontSize: 60, fontWeight: FontWeight.w900);
const headingText2 = TextStyle(fontSize: 50, fontWeight: FontWeight.w800);
const headingText3 = TextStyle(fontSize: 30, fontWeight: FontWeight.w500);
const headingText4 = TextStyle(fontSize: 20, fontWeight: FontWeight.w400);

const headingText6 = TextStyle(fontSize: 15, fontWeight: FontWeight.w300);
