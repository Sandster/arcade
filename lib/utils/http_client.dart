import 'dart:convert';
import 'dart:io';

import 'package:arcade/utils/constants.dart';
import 'package:arcade/utils/request_type.dart';
import 'package:arcade/utils/task.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

FirebaseAuth auth = FirebaseAuth.instance;
const String baseUrl = Constants.BASE_URL;

class HttpClient {
  String path;
  String query;
  RequestType type;
  dynamic body;
  bool isAuthenticated;

  HttpClient(
      {required this.path,
      this.query = '',
      this.type = RequestType.GET,
      this.body,
      this.isAuthenticated = false});

  Future<Task> sendRequest() async {
    final Map<String, String> headers = {};
    var url = Uri.parse(baseUrl + path + query);
    isAuthenticated ? headers['Authorization'] = await _getToken() : null;

    dynamic response;

    try {
      //check request type
      switch (type) {
        case RequestType.POST:
          headers['Content-Type'] = 'application/json';
          response = await http.post(url,
              headers: headers, body: body is String ? body : jsonEncode(body));
          break;
        case RequestType.PATCH:
          headers['Content-Type'] = 'application/json';
          response =
              await http.patch(url, headers: headers, body: body is String ? body : jsonEncode(body));
          break;
        case RequestType.DELETE:
          response = await http.delete(url, headers: headers);
          break;
        case RequestType.GET:
          response = await http.get(url, headers: headers);
          break;
      }

      final responseBody = jsonDecode(response.body);

      switch (response.statusCode) {
        case 200:
          return Task(
              success: responseBody['success'],
              body: responseBody,
              message: responseBody['message']);
        case 500:
          return Task(
              success: false,
              body: responseBody,
              message: 'Internal server error');
        case 400:
          return Task(
              success: false,
              body: responseBody,
              message: 'Bad request, check parameters');
        default:
          return Task(success: true, body: responseBody);
      }
    } on SocketException {
      return Task(message: 'No internet connection', body: {}, success: false);
    } catch (error) {
      debugPrint(error.toString());
      return Task(
          message: 'HTTP error, check logs for more information',
          body: {},
          success: false);
    }
  }

  Future<String> _getToken() async {
    final now = DateTime.now();
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token') ?? '';
    final iat = prefs.getInt('token_iat') ?? 0;
    DateTime issue = DateTime.fromMillisecondsSinceEpoch(iat);
    final difference = now.difference(issue).inMinutes;

    //if token is more that 50 minutes old
    if (difference >= 50 || token == '') {
      token = await _generateToken();
      final prefs = await SharedPreferences.getInstance();
      prefs.setString('token', token);
      prefs.setInt('token_iat', DateTime.now().millisecondsSinceEpoch);
    }
    return 'Bearer $token';
  }

  Future<String> _generateToken() async {
    User? user = auth.currentUser;
    String token = '';
    if (user != null) {
      token = await user.getIdToken(true);
    }
    return token;
  }
}
