class Task {
  final String message;
  final bool success;
  final Map<String, dynamic> body;

  Task({this.message = '', this.success = true, required this.body});
}
