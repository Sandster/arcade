import 'package:flutter/material.dart';

const mobileBackgroundColor = Color.fromRGBO(57, 62, 70, 1);
const bodyBackgroundColor = Color.fromRGBO(47, 54, 64, 1);
const mobileBackgroundColorLight = Color.fromRGBO(99, 104, 113, 1);
const mobileBackgroundColorDark = Color.fromRGBO(19, 24, 31, 1);
const webBackgroundColor = Color.fromRGBO(18, 18, 18, 1);
const mobileSearchColor = Color.fromRGBO(38, 38, 38, 1);
const errorColor = Color.fromRGBO(217, 83, 83, 1.0);
const accentColor = Color.fromRGBO(0, 173, 181, 1);
const primaryColor = Colors.white;
const secondaryColor = Colors.grey;

const chipColor = Color.fromRGBO(28, 97, 104, 1.0);
const alternateChipColor = Color.fromRGBO(0, 151, 230, 1.0);
const activeColor = Color.fromRGBO(76, 209, 55, 1.0);

