import 'package:flutter/material.dart';

List<Map<String, dynamic>> payloadCast({required List<dynamic> data}) {
  List<Map<String, dynamic>> tempList = [];

  data.asMap().forEach((index, value) {
    tempList.add(data[index]);
  });

  return tempList;
}

class ListScrollBehaviour extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(
      BuildContext context, Widget child, ScrollableDetails details) {
    return child;
  }
}
