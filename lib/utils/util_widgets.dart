import 'package:arcade/utils/styles.dart';
import 'package:arcade/widgets/shimmer_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_overlay_loader/flutter_overlay_loader.dart';
import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

showSnackBar({required String message, required context}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text(message),
  ));
}

Future<void> openBrowserTab(String url) async {
  bool isActive = await _defaultBrowserActivated();
  if (isActive) {
    if (!await launch(url)) throw 'Could not launch $url';
  } else {
    await FlutterWebBrowser.openWebPage(
      url: url,
      customTabsOptions: const CustomTabsOptions(
        colorScheme: CustomTabsColorScheme.dark,
        shareState: CustomTabsShareState.on,
        instantAppsEnabled: false,
        showTitle: true,
        urlBarHidingEnabled: false,
      ),
      safariVCOptions: const SafariViewControllerOptions(
        barCollapsingEnabled: true,
        dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
        modalPresentationCapturesStatusBarAppearance: true,
      ),
    );
  }
}

Future<bool> _defaultBrowserActivated() async {
  final prefs = await SharedPreferences.getInstance();
  bool defaultBrowser = prefs.getBool('defaultBrowserEnabled') ?? false;
  return defaultBrowser;
}

showLoader(context) {
  Loader.show(context,
      progressIndicator: const CircularProgressIndicator(),
      isSafeAreaOverlay: false,
      isAppbarOverlay: true,
      isBottomBarOverlay: false,
      themeData: Theme.of(context).copyWith(
          colorScheme:
              ColorScheme.fromSwatch().copyWith(secondary: Colors.black38)),
      overlayColor: const Color(0x99E8EAF6));
}

Future<void> showAlert(
    {required context, required String title, required Widget body}) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: false, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: SingleChildScrollView(
          child: body,
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('Okay'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

handleListLoading({required bool initialRender, required CardType type}) {
  if (initialRender) {
    initialRender = false;
    return ShimmerPlaceholder(type: type);
  } else {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Lottie.asset('assets/animations/no_result.json',
              width: 150, height: 150, fit: BoxFit.fill),
          const Text(
            'No Results Found',
            style: headingText4,
          )
        ],
      ),
    );
  }
}
