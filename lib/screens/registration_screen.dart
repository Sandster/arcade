import 'package:arcade/models/preferences.dart';
import 'package:arcade/screens/preferences_screen.dart';
import 'package:arcade/services/authentication_service.dart';
import 'package:arcade/utils/dimensions.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/loading_button.dart';
import 'package:arcade/widgets/textfield_input.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();
  final RoundedLoadingButtonController _registerButtonController =
      RoundedLoadingButtonController();

  final _formKey = GlobalKey<FormState>();

  bool isPasswordMatch = true;

  @override
  void dispose() {
    super.dispose();
    _usernameController.dispose();
    _passwordController.dispose();
    _emailController.dispose();
    _confirmPassword.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Form(
            key: _formKey,
            child: CustomScrollView(
              slivers: [
                SliverFillRemaining(
                  hasScrollBody: false,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Row(
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: const Icon(
                                  Icons.arrow_back,
                                  size: 25.0,
                                )),
                            const Text(
                              'Register',
                              style: headingText2,
                            )
                          ],
                        ),
                      ),
                      Flexible(child: Container(), flex: 2),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(50),
                        child: Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Lottie.asset('assets/animations/register.json',
                              width: 120, height: 120, fit: BoxFit.fill),
                        ),
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                      TextFieldInput(
                        controller: _usernameController,
                        inputType: TextInputType.text,
                        icon: Icons.verified_user,
                        hint: 'Enter username',
                        label: 'Username',
                        inputAction: TextInputAction.next,
                        validationCallback:
                            ValidationBuilder().required().build(),
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                      TextFieldInput(
                        controller: _emailController,
                        inputType: TextInputType.emailAddress,
                        icon: Icons.mail,
                        hint: 'Enter email',
                        label: 'Email',
                        inputAction: TextInputAction.next,
                        validationCallback: ValidationBuilder().email().build(),
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                      TextFieldInput(
                        isPassword: true,
                        controller: _passwordController,
                        inputType: TextInputType.text,
                        icon: Icons.password_sharp,
                        hint: 'Enter Password',
                        label: 'Password',
                        inputAction: TextInputAction.next,
                        validationCallback: ValidationBuilder()
                            .required()
                            // .regExp(
                            //     RegExp(
                            //         r'/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/'),
                            //     'Should be a combination of letters, digits and special characters')
                            .minLength(6)
                            .build(),
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                      TextFieldInput(
                        onChange: (text) {
                          text != _passwordController.text
                              ? setState(() => isPasswordMatch = false)
                              : setState(() => isPasswordMatch = true);
                        },
                        errorText:
                            !isPasswordMatch ? 'Passwords do not match' : null,
                        isPassword: true,
                        controller: _confirmPassword,
                        inputType: TextInputType.text,
                        icon: Icons.password_sharp,
                        hint: 'Confirm Password',
                        label: 'Confirm',
                        inputAction: TextInputAction.done,
                        validationCallback:
                            ValidationBuilder().required().build(),
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                      LoadingButton(
                          controller: _registerButtonController,
                          text: 'Register',
                          onPressed: () async {
                            _formKey.currentState!.validate()
                                ? await register()
                                : _registerButtonController.reset();
                          }),
                      Flexible(child: Container(), flex: 2),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  register() async {
    Map<String, dynamic> userData = {
      'username': _usernameController.text,
      'email': _emailController.text,
      'password': _passwordController.text
    };

    Task registration = await registerUser(registerData: userData);
    if (!registration.success) {
      showSnackBar(message: registration.message, context: context);
      _registerButtonController.reset();
    } else {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => ChangeNotifierProvider(
                create: (context) => PreferencesModel(),
                child: const PreferencesScreen(initialStage: true))),
      );
    }
  }
}
