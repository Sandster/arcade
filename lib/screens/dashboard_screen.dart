import 'dart:convert';

import 'package:arcade/models/browse.dart';
import 'package:arcade/models/deal.dart';
import 'package:arcade/models/giveaway.dart';
import 'package:arcade/models/profile.dart';
import 'package:arcade/screens/browse/browse_screen.dart';
import 'package:arcade/screens/deals/deals_screen.dart';
import 'package:arcade/screens/profile/profile_screen.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'giveaways/giveaway_screen.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  Map<String, dynamic> _userInfo = {};
  final PersistentTabController _tabController =
      PersistentTabController(initialIndex: 0);

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  void initState() {
    super.initState();
    _loadUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/dash_cover.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20.0, horizontal: 10.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeNotifierProvider(
                                  create: (context) => ProfileModel(),
                                  child: const ProfileScreen())),
                        );
                      },
                      child: SvgPicture.asset(
                        'assets/images/user.svg',
                        width: 80,
                        height: 80.0,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        'Welcome back!',
                        style: headingText4,
                      ),
                      Text(
                        _userInfo.isNotEmpty
                            ? _userInfo['username']
                            : 'Loading...',
                        style: headingText4,
                      ),
                    ],
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 20.0),
                          child: IconButton(
                            icon: const Icon(
                              Icons.search,
                              size: 25.0,
                            ),
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        ChangeNotifierProvider(
                                            create: (context) => BrowseModel(),
                                            child: const BrowseScreen())),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: const BoxDecoration(
                  color: bodyBackgroundColor,
                  borderRadius: BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 50.0),
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    child: loadTabs(),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  PersistentTabView loadTabs() {
    return PersistentTabView(
      context,
      controller: _tabController,
      screens: _navScreens(),
      items: _navBarItems(),
      confineInSafeArea: true,
      backgroundColor: bodyBackgroundColor,
      // Default is Colors.white.
      handleAndroidBackButtonPress: true,
      // Default is true.
      resizeToAvoidBottomInset: true,
      // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: false,
      // Default is true.
      hideNavigationBarWhenKeyboardShows: false,
      // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: mobileBackgroundColor,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: const ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: const ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style1, // Choose the nav bar style with this property.
    );
  }

  List<Widget> _navScreens() {
    return [
      ChangeNotifierProvider(
          create: (context) => DealsModel(), child: const DealsScreen()),
      ChangeNotifierProvider(
          create: (context) => GiveawaysModel(), child: const GiveawayScreen()),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.local_offer_rounded),
        title: ("Deals"),
        activeColorPrimary: accentColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.recommend_rounded),
        title: ("Giveaways"),
        activeColorPrimary: accentColor,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  Future<Map<String, dynamic>> _loadUserData() async {
    final prefs = await SharedPreferences.getInstance();
    String user = prefs.getString('user') ?? '';
    Map<String, dynamic> userData = jsonDecode(user);
    setState(() => _userInfo = userData);
    return userData;
  }
}
