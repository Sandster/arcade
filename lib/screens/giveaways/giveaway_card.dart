import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';

class GiveawayCard extends StatelessWidget {
  final String imageUrl;
  final String header;
  final String body;
  final String publishedDate;
  final List<String>? platforms;
  final bool activeIndicator;
  final VoidCallback onTapped;

  const GiveawayCard(
      {Key? key,
      required this.imageUrl,
      required this.header,
      required this.body,
      required this.activeIndicator,
      required this.publishedDate,
      required this.onTapped,
      this.platforms})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 10,
        child: InkWell(
          onTap: onTapped,
          child: Stack(
            children: [
              Ink.image(
                image: NetworkImage(
                  imageUrl,
                ),
                fit: BoxFit.fill,
                height: 200,
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Container(
                  color: bodyBackgroundColor.withOpacity(0.7),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              header.length > 50
                                  ? header.substring(0, 50) + '...'
                                  : header,
                              style: const TextStyle(
                                  fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                            Expanded(child: Container()),
                            Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: CircleAvatar(
                                radius: 5.0,
                                backgroundColor:
                                    activeIndicator ? activeColor : errorColor,
                              ),
                            ),
                          ],
                        ),
                        Text(body.length > 120
                            ? body.substring(0, 120) + '...'
                            : body),
                        const SizedBox(
                          height: 5.0,
                        ),
                        Text('Published at ' +
                            publishedDate.split(' ').elementAt(0)),
                        _generatePlatformChips(),
                        //Text('PC, Playstation 4, Android, iOS'),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _generatePlatformChips() => Wrap(
        children: platforms!
            .map((platform) => Chip(
                  label: Text(platform),
                  labelStyle: const TextStyle(
                    fontSize: 10.0,
                    color: Colors.white,
                  ),
                  backgroundColor: alternateChipColor,
                ))
            .toList(),
      );
}
