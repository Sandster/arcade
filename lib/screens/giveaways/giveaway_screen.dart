import 'package:arcade/models/giveaway.dart';
import 'package:arcade/screens/giveaways/giveaway_card.dart';
import 'package:arcade/services/giveaways_service.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/helpers.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/shimmer_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class GiveawayScreen extends StatefulWidget {
  const GiveawayScreen({Key? key}) : super(key: key);

  @override
  _GiveawayScreenState createState() => _GiveawayScreenState();
}

class _GiveawayScreenState extends State<GiveawayScreen> {
  String _type = 'game';
  bool _initialRender = true;
  int _page = 1;
  bool _isListEnd = false;

  final ScrollController _listController = ScrollController();

  @override
  void initState() {
    super.initState();
    _listController.addListener(_scrollListener);
    _loadGiveaways(false);
  }

  @override
  void dispose() {
    super.dispose();
    _listController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: bodyBackgroundColor,
      ),
      child: Column(
        children: [
          //chips
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                _renderChip(
                    label: 'Game',
                    onSelect: (isSelected) =>
                        _onChipSelect(isSelected: isSelected, type: 'game'),
                    selected: _type == 'game' ? true : false),
                _renderChip(
                    label: 'DLC',
                    onSelect: (isSelected) =>
                        _onChipSelect(isSelected: isSelected, type: 'loot'),
                    selected: _type == 'loot' ? true : false),
                _renderChip(
                    label: 'Beta',
                    onSelect: (isSelected) =>
                        _onChipSelect(isSelected: isSelected, type: 'beta'),
                    selected: _type == 'beta' ? true : false),
              ],
            ),
          ),
          Consumer<GiveawaysModel>(
            builder: (context, giveawayModel, child) {
              return Expanded(
                child: giveawayModel.giveaways.isNotEmpty
                    ? ScrollConfiguration(
                        behavior: ListScrollBehaviour(),
                        child: ListView.builder(
                            controller: _listController,
                            padding: const EdgeInsets.all(8),
                            itemCount: giveawayModel.giveaways.length,
                            itemBuilder: (context, index) {
                              return GiveawayCard(
                                onTapped: () {
                                  openBrowserTab(
                                      giveawayModel.giveaways[index].url!);
                                },
                                imageUrl: giveawayModel.giveaways[index].image!,
                                activeIndicator:
                                    giveawayModel.giveaways[index].status!
                                        ? true
                                        : false,
                                header: giveawayModel.giveaways[index].title!,
                                body:
                                    giveawayModel.giveaways[index].description!,
                                publishedDate: giveawayModel
                                    .giveaways[index].publishedDate!,
                                platforms:
                                    giveawayModel.giveaways[index].platforms!,
                              );
                            }),
                      )
                    : handleListLoading(
                        initialRender: _initialRender, type: CardType.giveaway),
              );
            },
          )
        ],
      ),
    );
  }

  _scrollListener() {
    if (_listController.offset >= _listController.position.maxScrollExtent &&
        !_listController.position.outOfRange) {
      if (!_isListEnd) {
        _page++;
        _loadGiveaways(true);
      }
    }
  }

  _onChipSelect({required bool isSelected, required String type}) {
    if (isSelected) {
      _page = 1;
      _isListEnd = false;
      setState(() {
        _type = type;
        _initialRender = true;
      });
      _loadGiveaways(false);
      _listController.animateTo(_listController.position.minScrollExtent,
          duration: const Duration(seconds: 2), curve: Curves.fastOutSlowIn);
    }
  }

  _loadGiveaways(bool isPaginate) async {
    Task giveaways = await getAllGiveaways(
        context: context, type: _type, page: _page, paginate: isPaginate);
    if (!giveaways.success) {
      showSnackBar(message: giveaways.message, context: context);
    } else {
      bool areGiveawaysEmpty =
          Provider.of<GiveawaysModel>(context, listen: false).giveaways.isEmpty;
      _page > 1 && areGiveawaysEmpty ? _isListEnd = true : _isListEnd = false;
    }
  }

  List<String> extractPlatforms(String platforms) => platforms.split(', ');

  Widget _renderChip(
          {required String label,
          required ValueSetter<bool> onSelect,
          required bool selected}) =>
      Transform(
        transform: Matrix4.identity()..scale(1.2),
        child: FilterChip(
          label: Text(label),
          labelStyle: const TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.red,
          ),
          backgroundColor: Colors.red.withOpacity(0.1),
          onSelected: onSelect,
          selected: selected,
          checkmarkColor: Colors.red,
          selectedColor: Colors.red.withOpacity(0.25),
        ),
      );
}
