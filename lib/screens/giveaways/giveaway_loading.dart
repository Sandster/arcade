import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class GiveawayLoading extends StatelessWidget {
  const GiveawayLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: Shimmer.fromColors(
        baseColor: Colors.grey[700]!,
        highlightColor: Colors.grey[600]!,
        child: Container(
          height: 200.0,
          child: Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAlias,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Container(),
          ),
        ),
      ),
    );
  }
}
