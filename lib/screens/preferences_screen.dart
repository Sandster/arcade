import 'package:arcade/models/preferences.dart';
import 'package:arcade/models/profile.dart';
import 'package:arcade/screens/dashboard_screen.dart';
import 'package:arcade/screens/profile/profile_screen.dart';
import 'package:arcade/services/preferences_service.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/dimensions.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/dropdown_input.dart';
import 'package:arcade/widgets/loading_button.dart';
import 'package:arcade/widgets/removable_chip.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class PreferencesScreen extends StatefulWidget {
  final bool initialStage;

  const PreferencesScreen({Key? key, required this.initialStage})
      : super(key: key);

  @override
  _PreferencesScreenState createState() => _PreferencesScreenState();
}

class _PreferencesScreenState extends State<PreferencesScreen> {
  final RoundedLoadingButtonController _continueController =
      RoundedLoadingButtonController();
  final RoundedLoadingButtonController _cancelController =
      RoundedLoadingButtonController();

  @override
  void initState() {
    super.initState();
    loadPreferences();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  'Personalise',
                  style: headingText2,
                ),
              ),
              const SizedBox(
                height: 60.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    children: [
                      Lottie.asset('assets/animations/preferences.json',
                          width: 120, height: 120, fit: BoxFit.fill),
                      const SizedBox(
                        height: formGap,
                      ),
                      Text(
                        widget.initialStage
                            ? '(You can always change these settings later on)'
                            : 'Make arcade yours',
                        style: headingText4,
                      ),
                      const SizedBox(
                        height: formGap,
                      ),
                    ],
                  ),
                ],
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _sectionHeader(title: 'Genre'),
                      const SizedBox(
                        height: formGap,
                      ),
                      Consumer<PreferencesModel>(
                        builder: (context, preferencesModel, child) {
                          return DropdownInput(
                              icon: const Icon(Icons.arrow_downward),
                              inputData: preferencesModel.allGenres,
                              isDisabled: preferencesModel.allGenres.isEmpty
                                  ? true
                                  : false,
                              placeholderText: 'Select Genre',
                              value: preferencesModel.currentGenre,
                              onChange: (dynamic newValue) {
                                preferencesModel.currentGenre = newValue;
                                !preferencesModel.selectedGenres
                                        .contains(newValue)
                                    ? preferencesModel.addGenre(newValue)
                                    : null;
                              });
                        },
                      ),
                      Padding(
                          padding:
                              const EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: Consumer<PreferencesModel>(
                            builder: (context, preferenceModel, child) {
                              return _renderChips(
                                  dataList: preferenceModel.selectedGenres,
                                  isGenre: true);
                            },
                          )),
                      _sectionHeader(title: 'Platform'),
                      const SizedBox(
                        height: formGap,
                      ),
                      Consumer<PreferencesModel>(
                        builder: (context, preferencesModel, child) {
                          return DropdownInput(
                              icon: const Icon(Icons.arrow_downward),
                              isDisabled: preferencesModel.allPlatforms.isEmpty
                                  ? true
                                  : false,
                              inputData: preferencesModel.allPlatforms,
                              placeholderText: 'Select Platform',
                              value: preferencesModel.currentPlatform,
                              onChange: (dynamic newValue) {
                                preferencesModel.currentPlatform = newValue;
                                !preferencesModel.selectedPlatforms
                                        .contains(newValue)
                                    ? preferencesModel.addPlatform(newValue)
                                    : null;
                              });
                        },
                      ),
                      Padding(
                          padding:
                              const EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: Consumer<PreferencesModel>(
                            builder: (context, preferencesModel, child) {
                              return _renderChips(
                                  dataList: preferencesModel.selectedPlatforms,
                                  isGenre: false);
                            },
                            // child: _renderChips(dataList: _selectedPlatforms)),
                          )),
                      const SizedBox(
                        height: 20.0,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Expanded(
                              child: LoadingButton(
                                onPressed: () {
                                  var provider = Provider.of<PreferencesModel>(
                                      context,
                                      listen: false);

                                  if (provider.allPlatforms.isNotEmpty &&
                                      provider.allGenres.isNotEmpty) {
                                    updatePreferences();
                                  }
                                },
                                controller: _continueController,
                                text: 'Continue',
                              ),
                            ),
                            const SizedBox(
                              width: 10.0,
                            ),
                            Expanded(
                              child: LoadingButton(
                                onPressed: () {
                                  _cancelController.reset();
                                  widget.initialStage
                                      ? Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  const DashboardScreen()),
                                          (Route<dynamic> route) => false,
                                        )
                                      : Navigator.pop(context);
                                },
                                controller: _cancelController,
                                text: widget.initialStage ? 'Skip' : 'Cancel',
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _sectionHeader({required String title}) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Expanded(
            child: Divider(
              indent: 20.0,
              endIndent: 10.0,
              color: primaryColor,
            ),
          ),
          Text(
            title,
            style: headingText4,
          ),
          const Expanded(
            child: Divider(
              indent: 10.0,
              endIndent: 20.0,
              color: primaryColor,
            ),
          ),
        ],
      );

  Widget _renderChips({required List dataList, required bool isGenre}) => Wrap(
        spacing: 5.0,
        children: dataList
            .map((data) => RemovableChip(
                label: data.name,
                onDelete: () {
                  var provider =
                      Provider.of<PreferencesModel>(context, listen: false);

                  isGenre
                      ? provider.removeGenre(data)
                      : provider.removePlatform(data);
                  //setState(() => dataList.remove(data));
                }))
            .toList(),
      );

  loadPreferences() async {
    Task preferencesTask = await getAllPreferences(context: context);

    !preferencesTask.success
        ? showSnackBar(message: preferencesTask.message, context: context)
        : null;
  }

  updatePreferences() async {
    Task preferenceUpdateTask = await updateAllPreferences(context: context);
    if (preferenceUpdateTask.success) {
      _continueController.reset();
      widget.initialStage
          ? Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => const DashboardScreen()),
              (Route<dynamic> route) => false,
            )
          : Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                  builder: (context) => ChangeNotifierProvider(
                      create: (context) => ProfileModel(),
                      child: ProfileScreen())),
            );
    } else {
      showSnackBar(message: preferenceUpdateTask.message, context: context);
    }
  }
}
