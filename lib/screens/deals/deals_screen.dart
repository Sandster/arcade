import 'package:arcade/models/deal.dart';
import 'package:arcade/screens/deals/deal_card.dart';
import 'package:arcade/services/deals_service.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/dimensions.dart';
import 'package:arcade/utils/helpers.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/shimmer_placeholder.dart';
import 'package:arcade/widgets/textfield_input.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:provider/provider.dart';

class DealsScreen extends StatefulWidget {
  const DealsScreen({Key? key}) : super(key: key);

  @override
  _DealsScreenState createState() => _DealsScreenState();
}

class _DealsScreenState extends State<DealsScreen> {
  Map<String, dynamic> _storeData = {};

  double _minimum = 0;
  double _maximum = 0;

  bool _initialRender = true;
  bool _isListEnd = false;
  bool _saleSwitchValue = false;

  int _page = 1;

  final TextEditingController _minimumPriceController = TextEditingController();
  final TextEditingController _maximumPriceController = TextEditingController();
  final ScrollController _listController = ScrollController();

  @override
  void initState() {
    super.initState();
    _listController.addListener(_scrollListener);
    _loadDeals(false);
  }

  @override
  void dispose() {
    super.dispose();
    _listController.dispose();
    _minimumPriceController.dispose();
    _maximumPriceController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: bodyBackgroundColor,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  const Text(
                    'Filter',
                    style: headingText4,
                  ),
                  IconButton(
                      onPressed: () {
                        _showFilterDialog();
                      },
                      icon: const Icon(Icons.filter_alt))
                ],
              )
            ],
          ),
          Consumer<DealsModel>(builder: (context, dealModel, child) {
            return Expanded(
              child: dealModel.deals.isNotEmpty
                  ? ScrollConfiguration(
                      behavior: ListScrollBehaviour(),
                      child: ListView.builder(
                          controller: _listController,
                          padding: const EdgeInsets.all(8),
                          itemCount: dealModel.deals.length,
                          itemBuilder: (context, index) {
                            return DealCard(
                                onTapped: () async {
                                  await _getStoreData(int.parse(
                                      dealModel.deals[index].storeId!));
                                  showAlert(
                                    context: context,
                                    title: 'Available at',
                                    body: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        Container(
                                          child: FittedBox(
                                            child: Image.network(
                                                _storeData['image']),
                                            fit: BoxFit.contain,
                                          ),
                                          width: 60.0,
                                          height: 60.0,
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20.0),
                                          child: Text(_storeData['name']),
                                        )
                                      ],
                                    ),
                                  );
                                },
                                title: dealModel.deals[index].title!,
                                salePrice: dealModel.deals[index].salePrice!,
                                originalPrice:
                                    dealModel.deals[index].normalPrice!,
                                imageUrl: dealModel.deals[index].thumbnail!,
                                date: dealModel.deals[index].lastUpdate!,
                                rating: dealModel.deals[index].rating!);
                          }),
                    )
                  : handleListLoading(
                      initialRender: _initialRender, type: CardType.deal),
            );
          })
        ],
      ),
    );
  }

  _scrollListener() {
    if (_listController.offset >= _listController.position.maxScrollExtent &&
        !_listController.position.outOfRange) {
      if (!_isListEnd) {
        _page++;
        _loadDeals(true);
      }
    }
  }

  Future<void> _showFilterDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: mobileBackgroundColor,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          contentPadding: const EdgeInsets.all(10.0),
          elevation: 24.0,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text('Filter'),
            ],
          ),
          content: StatefulBuilder(
            builder: (context, setState) {
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextFieldInput(
                        controller: _minimumPriceController,
                        inputType: TextInputType.number,
                        icon: Icons.attach_money_sharp,
                        hint: 'Minimum price',
                        label: 'Minimum',
                        validationCallback: ValidationBuilder().build()),
                    const SizedBox(
                      height: formGap,
                    ),
                    TextFieldInput(
                        controller: _maximumPriceController,
                        inputType: TextInputType.number,
                        icon: Icons.attach_money_sharp,
                        hint: 'Maximum price',
                        label: 'Maximum',
                        validationCallback: ValidationBuilder().build()),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Padding(
                          padding: EdgeInsets.only(left: 5.0),
                          child: Text('Sale items only'),
                        ),
                        Switch.adaptive(
                            value: _saleSwitchValue,
                            onChanged: (value) {
                              setState(() {
                                _saleSwitchValue = value;
                              });
                            }),
                      ],
                    )
                  ],
                ),
              );
            },
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Apply'),
              onPressed: () {
                _maximumPriceController.text.isNotEmpty
                    ? _maximum = double.parse(_maximumPriceController.text)
                    : null;
                _minimumPriceController.text.isNotEmpty
                    ? _minimum = double.parse(_minimumPriceController.text)
                    : null;
                _listController.animateTo(
                    _listController.position.minScrollExtent,
                    duration: const Duration(seconds: 2),
                    curve: Curves.fastOutSlowIn);
                Navigator.of(context).pop();
                _loadDeals(false);
                //setState(() => _loadDeals(false));
              },
            ),
            TextButton(
              child: const Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  _loadDeals(bool isPaginate) async {
    Task deals = await getAllDeals(
        context: context,
        page: _page,
        minimumPrice: _minimum,
        maximumPrice: _maximum,
        isSale: _saleSwitchValue,
        paginate: isPaginate);

    if (!deals.success) {
      showSnackBar(message: deals.message, context: context);
    } else {
      bool areDealEmpty =
          Provider.of<DealsModel>(context, listen: false).deals.isEmpty;
      _page > 1 && areDealEmpty ? _isListEnd = true : _isListEnd = false;
    }
  }

  _getStoreData(int id) async {
    Task store = await getStoreInfo(id);
    if (!store.success) {
      showSnackBar(message: store.message, context: context);
    } else {
      Map<String, dynamic> body = store.body;
      List<dynamic> payload = body['payload'];
      List<Map<String, dynamic>> castData = payloadCast(data: payload);

      setState(() {
        _storeData = castData[0];
      });
    }
  }
}
