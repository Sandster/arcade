import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';

class DealCard extends StatelessWidget {
  final String title;
  final String salePrice;
  final String originalPrice;
  final String imageUrl;
  final int date;
  final String rating;
  final VoidCallback onTapped;

  const DealCard(
      {Key? key,
      required this.title,
      required this.salePrice,
      required this.originalPrice,
      required this.imageUrl,
      required this.date,
      required this.rating,
      required this.onTapped})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 10,
        child: InkWell(
          onTap: onTapped,
          child: Row(
            children: [
              Ink.image(
                image: NetworkImage(
                  imageUrl,
                ),
                fit: BoxFit.contain,
                width: 140,
              ),
              Expanded(
                child: Container(
                  color: mobileBackgroundColor,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const SizedBox(height: 20.0),
                          Text(title,
                              style: const TextStyle(
                                  fontSize: 20.0, fontWeight: FontWeight.bold)),
                          const Expanded(
                              child: Divider(
                            thickness: 2.0,
                          )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5.0, bottom: 5.0),
                                    child: Text('Sale Price: \$$salePrice',
                                        style: const TextStyle(
                                            fontSize: 20.0,
                                            fontWeight: FontWeight.w500)),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 5.0, bottom: 5.0),
                                    child: Text('Original Price: \$$originalPrice'),
                                  ),
                                  Text('Issued Date : ${changeTimestamp(date)}'),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(rating,
                                      style: const TextStyle(
                                          fontSize: 40.0,
                                          fontWeight: FontWeight.w500)),
                                  const Icon(Icons.star,
                                      color: Colors.amber, size: 20.0)
                                ],
                              )
                            ],
                          ),
                          const SizedBox(height: 20.0),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String changeTimestamp(int timestamp) {
    DateTime date = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
    return '${date.day}/${date.month}/${date.year}';
  }
}
