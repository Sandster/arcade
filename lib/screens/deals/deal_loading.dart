import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class DealLoading extends StatelessWidget {
  DealLoading({Key? key}) : super(key: key);

  final Color base = Colors.grey[700]!;
  final Color highlight = Colors.grey[600]!;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 20.0),
      child: Container(
        decoration: BoxDecoration(
            color: mobileBackgroundColor,
            border: Border.all(
              color: Colors.grey[700]!,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(10.0))),
        height: 200.0,
        child: Row(
          children: [
            Shimmer.fromColors(
              baseColor: base,
              highlightColor: highlight,
              child: Padding(
                padding: const EdgeInsets.only(left: 20.0),
                child: Container(
                  width: 140.0,
                  color: Colors.grey,
                ),
              ),
            ),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 8.0, 20.0, 0.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Shimmer.fromColors(
                    baseColor: base,
                    highlightColor: highlight,
                    child: Container(
                      color: Colors.grey,
                      width: double.infinity,
                      height: 30.0,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 80.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Shimmer.fromColors(
                          baseColor: base,
                          highlightColor: highlight,
                          child: Container(
                            color: Colors.grey,
                            width: double.infinity,
                            height: 20.0,
                          ),
                        ),
                        Shimmer.fromColors(
                          baseColor: base,
                          highlightColor: highlight,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Container(
                              color: Colors.grey,
                              width: double.infinity,
                              height: 20.0,
                            ),
                          ),
                        ),
                        Shimmer.fromColors(
                          baseColor: base,
                          highlightColor: highlight,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 5.0, bottom: 5.0),
                            child: Container(
                              color: Colors.grey,
                              width: double.infinity,
                              height: 20.0,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }
}
