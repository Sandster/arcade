import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class StoreItem extends StatelessWidget {
  final String storeName;
  final VoidCallback tap;

  const StoreItem({Key? key, required this.storeName, required this.tap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
        leading: SvgPicture.asset(
          'assets/images/ic_store.svg',
          width: 40,
          height: 40,
          fit: BoxFit.fill,
        ),
        title: Text(storeName),
        subtitle: const Text('Click to access store'),
        onTap: tap,
        trailing: const Icon(Icons.arrow_forward_ios));
  }
}
