import 'package:arcade/models/browse.dart';
import 'package:arcade/models/game.dart';
import 'package:arcade/screens/browse/store_item.dart';
import 'package:arcade/services/game_service.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/description_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:like_button/like_button.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

class GameItem extends StatefulWidget {
  final int id;
  final Function()? refreshWishlist;

  const GameItem({Key? key, required this.id, this.refreshWishlist})
      : super(key: key);

  @override
  State<GameItem> createState() => _GameItemState();
}

class _GameItemState extends State<GameItem> {

  @override
  void initState() {
    super.initState();
    _loadGameData();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<GameModel>(builder: (context, gameModel, child) {
      return SingleChildScrollView(
        child: gameModel.game != null
            ? Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Stack(
                    children: [
                      Ink.image(
                        image: NetworkImage(gameModel.game!.image!),
                        fit: BoxFit.cover,
                        height: 200,
                      ),
                      Positioned(
                          right: 0,
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 10.0, right: 10.0),
                              child: Row(
                                children: [
                                  Text(gameModel.game!.rating!,
                                      style: const TextStyle(
                                          fontSize: 40.0,
                                          fontWeight: FontWeight.w500)),
                                  const Icon(Icons.star,
                                      color: Colors.amber, size: 20.0)
                                ],
                              ))),
                      Positioned(
                          right: 0,
                          bottom: 0,
                          child: Padding(
                            padding:
                                const EdgeInsets.only(top: 10.0, right: 10.0),
                            child: Text(gameModel.game!.esrbRating!,
                                style: const TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w800)),
                          )),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 20.0, left: 20.0, right: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                gameModel.game!.name!,
                                style: const TextStyle(
                                    fontSize: 25.0,
                                    fontWeight: FontWeight.w700),
                              ),
                              LikeButton(
                                isLiked: gameModel.wishlistAvailability,
                                size: 25.0,
                                onTap: (value) async {
                                  bool success = await manipulateWishlist();
                                  widget.refreshWishlist != null
                                      ? widget.refreshWishlist!()
                                      : null;
                                  return success ? !value : value;
                                },
                              ),
                            ],
                          ),
                        ),
                        DescriptionText(text: gameModel.game!.description!),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text('Release : ${gameModel.game!.releaseDate!}',
                                style: const TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w800)),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/ic_metacritic.svg',
                                  width: 40,
                                  height: 40,
                                  fit: BoxFit.fill,
                                ),
                                Text('${gameModel.game!.metacriticRating!}%',
                                    style: const TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.w800)),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          children: const [
                            Expanded(
                                child: Divider(
                              endIndent: 20.0,
                            )),
                            Text('Genres'),
                            Expanded(
                                child: Divider(
                              indent: 20.0,
                            )),
                          ],
                        ),
                        Wrap(
                          children: _showChips(false),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: const [
                            Expanded(
                                child: Divider(
                              endIndent: 20.0,
                            )),
                            Text('Platforms'),
                            Expanded(
                                child: Divider(
                              indent: 20.0,
                            )),
                          ],
                        ),
                        Wrap(
                          children: _showChips(true),
                        ),
                        Row(
                          children: const [
                            Text('Stores'),
                            Expanded(
                                child: Divider(
                              indent: 20.0,
                            )),
                          ],
                        ),
                        Container(
                          height: 150.0,
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: gameModel.game!.stores!.length,
                            itemBuilder: (context, index) {
                              return StoreItem(
                                  storeName:
                                      gameModel.game!.stores![index].name!,
                                  tap: () {
                                    //TODO browser conflict
                                    print(
                                        gameModel.game!.stores![index].domain!);
                                    openBrowserTab(
                                        gameModel.game!.stores![index].domain!);
                                  });
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )
            : Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Lottie.asset('assets/animations/loading.json',
                        width: 150, height: 150, fit: BoxFit.fill),
                    const Text(
                      'Loading',
                      style: headingText4,
                    )
                  ],
                ),
              ),
      );
    });
  }

  List<Widget> _showChips(bool isPlatform) {
    GameResult? game = Provider.of<GameModel>(context, listen: false).game;
    List<Widget> chips = [];
    dynamic iterateList = game!.platforms;
    !isPlatform ? iterateList = game.genres : null;
    iterateList.forEach((item) {
      chips.add(Chip(
        label: Text(item.name),
        labelStyle: const TextStyle(
          fontSize: 10.0,
          color: Colors.white,
        ),
        backgroundColor: Colors.lightBlue,
      ));
    });
    return chips;
  }

  Future<bool> manipulateWishlist() async {
    var provider = Provider.of<GameModel>(context, listen: false);

    if (provider.wishlistAvailability) {
      //currently added to wishlist so remove
      Task remove = await removeFromWishlist(context: context, id: widget.id);

      return remove.success;
    } else {
      //not available in wishlist so add
      Task add = await addToWishlist(context: context, game: provider.game!);
      return add.success;
    }
  }

  _loadGameData() async {
    Task gameDataTask = await getGameData(context: context, id: widget.id);
    !gameDataTask.success
        ? showSnackBar(message: gameDataTask.message, context: context)
        : null;
  }
}
