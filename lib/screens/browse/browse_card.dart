import 'package:arcade/models/browse.dart';
import 'package:arcade/utils/colors.dart';
import 'package:flutter/material.dart';

class BrowseCard extends StatelessWidget {
  final VoidCallback onTapped;
  final String imageUrl;
  final String name;
  final String releasedAt;
  final String rating;
  final List<Platform>? platforms;

  const BrowseCard(
      {Key? key,
      required this.imageUrl,
      required this.name,
      required this.releasedAt,
      required this.rating,
      required this.onTapped,
      this.platforms})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0, right: 10.0, bottom: 10.0),
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 10,
        child: InkWell(
          onTap: onTapped,
          child: Stack(
            children: [
              Ink.image(
                image: NetworkImage(
                  imageUrl,
                ),
                fit: BoxFit.cover,
                height: 200,
              ),
              Positioned(
                bottom: 0,
                right: 0,
                left: 0,
                child: Container(
                  color: Colors.grey.withOpacity(0.7),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  name,
                                  style: const TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text('Released at: $releasedAt'),
                                const SizedBox(
                                  height: 5.0,
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(rating,
                                    style: const TextStyle(
                                        fontSize: 40.0,
                                        fontWeight: FontWeight.w500)),
                                const Icon(Icons.star,
                                    color: Colors.amber, size: 20.0)
                              ],
                            ),
                          ],
                        ),
                        Wrap(
                          children: _generateChips(),
                        )
                        //_generatePlatformChips()
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _generateChips() {
    final children = <Widget>[];
    int platformShowCount;
    platforms!.length > 4
        ? platformShowCount = 4
        : platformShowCount = platforms!.length;
    for (var i = 0; i < platformShowCount; i++) {
      children.add(Chip(
        label: Text(
          i == 3 ? '...' : platforms![i].name!,
          overflow: TextOverflow.ellipsis,
        ),
        labelStyle: const TextStyle(
          fontSize: 10.0,
          color: Colors.white,
        ),
        backgroundColor: alternateChipColor,
      ));
    }
    return children;
  }

  // _generateChips() {
  //   final children = <Widget>[];
  //   int platformShowCount;
  //   platforms!.length > 4
  //       ? platformShowCount = 4
  //       : platformShowCount = platforms!.length;
  //   for (var i = 0; i < platformShowCount; i++) {
  //     children.add(Chip(
  //       label: Text(
  //         i == 3 ? '...' : platforms![i]['name'],
  //         overflow: TextOverflow.ellipsis,
  //       ),
  //       labelStyle: const TextStyle(
  //         fontSize: 10.0,
  //         color: Colors.white,
  //       ),
  //       backgroundColor: alternateChipColor,
  //     ));
  //   }
  //   return children;
  // }
}
