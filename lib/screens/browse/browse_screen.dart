import 'package:arcade/models/browse.dart';
import 'package:arcade/models/game.dart';
import 'package:arcade/screens/browse/browse_card.dart';
import 'package:arcade/screens/browse/game_item.dart';
import 'package:arcade/services/browse_service.dart';
import 'package:arcade/utils/helpers.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/floating_model.dart';
import 'package:arcade/widgets/shimmer_placeholder.dart';
import 'package:arcade/widgets/textfield_input.dart';
import 'package:flutter/material.dart';
import 'package:form_validator/form_validator.dart';
import 'package:provider/provider.dart';

class BrowseScreen extends StatefulWidget {
  const BrowseScreen({Key? key}) : super(key: key);

  @override
  _BrowseScreenState createState() => _BrowseScreenState();
}

class _BrowseScreenState extends State<BrowseScreen> {
  final ScrollController _listController = ScrollController();

  bool _initialRender = true;

  @override
  void initState() {
    super.initState();
    _loadDefaultGames();
  }

  @override
  void dispose() {
    super.dispose();
    _listController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        size: 25.0,
                      )),
                  const Text(
                    'Browse',
                    style: headingText2,
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextFieldInput(
                  inputType: TextInputType.text,
                  inputAction: TextInputAction.search,
                  icon: Icons.search,
                  hint: 'Search a game',
                  label: 'Search',
                  onSubmit: (value) {
                    value.isNotEmpty ? _searchGame(value) : null;
                  },
                  validationCallback: ValidationBuilder().required().build()),
            ),
            Consumer<BrowseModel>(builder: (context, browseModel, child) {
              return Expanded(
                child: browseModel.games.isNotEmpty
                    ? ScrollConfiguration(
                        behavior: ListScrollBehaviour(),
                        child: ListView.builder(
                            controller: _listController,
                            padding: const EdgeInsets.all(8),
                            itemCount: browseModel.games.length,
                            itemBuilder: (context, index) {
                              return BrowseCard(
                                  imageUrl: browseModel.games[index].image!,
                                  name: browseModel.games[index].name!,
                                  releasedAt:
                                      browseModel.games[index].releaseDate!,
                                  rating: browseModel.games[index].rating!,
                                  platforms:
                                      browseModel.games[index].platforms!,
                                  onTapped: () {
                                    showFloatingModalBottomSheet(
                                        context: context,
                                        builder: (context) =>
                                            ChangeNotifierProvider(
                                              create: (context) => GameModel(),
                                              child: GameItem(
                                                  id: browseModel
                                                      .games[index].id!),
                                            ));
                                  });
                            }),
                      )
                    : handleListLoading(
                        initialRender: _initialRender, type: CardType.browse),
              );
            })
          ],
        ),
      ),
    );
  }

  _searchGame(String value) async {
    setState(() => _initialRender = true);

    Task searchResult =
        await searchGame(context: context, searchCriteria: value);

    !searchResult.success
        ? showSnackBar(message: searchResult.message, context: context)
        : setState(() => _initialRender = false);
  }

  _loadDefaultGames() async {
    setState(() => _initialRender = true);
    Task games = await getAllGames(context: context);
    !games.success
        ? showSnackBar(message: games.message, context: context)
        : setState(() => _initialRender = false);
  }
}
