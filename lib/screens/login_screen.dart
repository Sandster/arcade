import 'package:arcade/screens/dashboard_screen.dart';
import 'package:arcade/screens/registration_screen.dart';
import 'package:arcade/services/authentication_service.dart';
import 'package:arcade/utils/dimensions.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/loading_button.dart';
import 'package:arcade/widgets/textfield_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:form_validator/form_validator.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final RoundedLoadingButtonController _loginButtonController =
      RoundedLoadingButtonController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    super.dispose();
    _emailController.dispose();
    _passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              width: double.infinity,
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Flexible(child: Container(), flex: 2),
                    SvgPicture.asset('assets/images/ic_logo_transparent.svg',
                        height: 200.0),
                    TextFieldInput(
                      controller: _emailController,
                      inputType: TextInputType.emailAddress,
                      icon: Icons.mail,
                      hint: 'Enter email',
                      label: 'Email',
                      inputAction: TextInputAction.next,
                      validationCallback:
                          ValidationBuilder().email().required().build(),
                    ),
                    const SizedBox(
                      height: formGap,
                    ),
                    TextFieldInput(
                      controller: _passwordController,
                      inputType: TextInputType.text,
                      icon: Icons.password_sharp,
                      hint: 'Enter Password',
                      label: 'Password',
                      isPassword: true,
                      validationCallback:
                          ValidationBuilder().minLength(6).build(),
                    ),
                    const SizedBox(height: formGap),
                    LoadingButton(
                      controller: _loginButtonController,
                      text: 'Login',
                      onPressed: () async {
                        _formKey.currentState!.validate()
                            ? await login(
                                email: _emailController.text,
                                password: _passwordController.text)
                            : _loginButtonController.reset();
                      },
                    ),
                    Flexible(child: Container(), flex: 2),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const RegistrationScreen()),
                        );
                      },
                      child: Container(
                        child: const Text(
                          'Register at Arcade',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        padding: const EdgeInsets.symmetric(vertical: 10),
                      ),
                    )
                  ],
                ),
              ))),
    );
  }

  login({required String email, required String password}) async {
    Task loginTaskResult = await loginUser(email: email, password: password);

    loginTaskResult.success
        ? Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => const DashboardScreen()),
          )
        : showSnackBar(message: loginTaskResult.message, context: context);
    _loginButtonController.reset();
  }
}
