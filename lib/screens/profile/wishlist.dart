import 'package:arcade/models/game.dart';
import 'package:arcade/models/profile.dart';
import 'package:arcade/screens/browse/browse_card.dart';
import 'package:arcade/screens/browse/game_item.dart';
import 'package:arcade/services/profile_service.dart';
import 'package:arcade/utils/helpers.dart';
import 'package:arcade/utils/task.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:arcade/widgets/floating_model.dart';
import 'package:arcade/widgets/shimmer_placeholder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Wishlist extends StatefulWidget {
  const Wishlist({Key? key}) : super(key: key);

  @override
  _WishlistState createState() => _WishlistState();
}

class _WishlistState extends State<Wishlist> {
  final ScrollController _listController = ScrollController();

  @override
  void initState() {
    super.initState();
    _loadWishList();
  }

  @override
  void dispose() {
    super.dispose();
    _listController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Consumer<ProfileModel>(
          builder: (context, profileModel, child) {
            return Expanded(
              child: profileModel.wishlist.isNotEmpty
                  ? ScrollConfiguration(
                      behavior: ListScrollBehaviour(),
                      child: ListView.builder(
                          controller: _listController,
                          padding: const EdgeInsets.all(8),
                          itemCount: profileModel.wishlist.length,
                          itemBuilder: (context, index) {
                            return BrowseCard(
                                imageUrl: profileModel.wishlist[index].image!,
                                name: profileModel.wishlist[index].name!,
                                releasedAt:
                                    profileModel.wishlist[index].releaseDate!,
                                rating: profileModel.wishlist[index].rating!,
                                platforms:
                                    profileModel.wishlist[index].platforms!,
                                onTapped: () {
                                  showFloatingModalBottomSheet(
                                      context: context,
                                      builder: (context) =>
                                          ChangeNotifierProvider(
                                            create: (context) => GameModel(),
                                            child: GameItem(
                                              id: profileModel
                                                  .wishlist[index].id!,
                                              refreshWishlist: _loadWishList,
                                            ),
                                          ));
                                });
                          }),
                    )
                  : handleListLoading(
                      initialRender: profileModel.initialRender,
                      type: CardType.giveaway),
            );
          },
        ),
      ],
    );
  }

  _loadWishList() async {
    Task wishlist = await getUserWishlist(context: context);
    !wishlist.success
        ? showSnackBar(message: wishlist.message, context: context)
        : null;
  }
}
