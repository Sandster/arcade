import 'package:arcade/models/profile.dart';
import 'package:arcade/screens/profile/settings.dart';
import 'package:arcade/screens/profile/wishlist.dart';
import 'package:arcade/services/profile_service.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  void initState() {
    super.initState();
    getUserData(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      size: 25.0,
                    )),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset('assets/images/user.svg', height: 150.0),
              ],
            ),
            Consumer<ProfileModel>(
              builder: (context, profileModel, child) {
                return Text(
                  profileModel.username,
                  style: headingText2,
                );
              },
            ),
            Consumer<ProfileModel>(
              builder: (context, profileModel, child) {
                return Text(
                  profileModel.email,
                  style: headingText6,
                );
              },
            ),
            const SizedBox(
              height: 20.0,
            ),
            Expanded(
              child: DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: PreferredSize(
                    preferredSize: const Size.fromHeight(40.0),
                    child: Container(
                      color: mobileBackgroundColor,
                      child: SafeArea(
                        child: Column(
                          children: <Widget>[
                            Expanded(child: Container()),
                            TabBar(
                              tabs: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: const [
                                    Text(
                                      'Settings',
                                      style: headingText4,
                                    ),
                                  ],
                                ),
                                const Text('Wishlist', style: headingText4)
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  body: const TabBarView(
                    children: <Widget>[
                      Settings(),
                      Wishlist(),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
