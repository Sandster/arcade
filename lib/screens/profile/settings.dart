import 'package:arcade/models/preferences.dart';
import 'package:arcade/models/profile.dart';
import 'package:arcade/screens/login_screen.dart';
import 'package:arcade/screens/preferences_screen.dart';
import 'package:arcade/services/authentication_service.dart';
import 'package:arcade/services/profile_service.dart';
import 'package:arcade/utils/colors.dart';
import 'package:arcade/utils/styles.dart';
import 'package:arcade/utils/util_widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  void initState() {
    super.initState();
    getUserSettings(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
              child: ListView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                children: [
                  _sectionHeading('General'),
                  _optionItem(
                      title: 'User Preferences',
                      icon: 'assets/images/preferences.svg',
                      subtitle: 'Click to change preferences',
                      tap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeNotifierProvider(
                                    create: (context) => PreferencesModel(),
                                    child: const PreferencesScreen(
                                      initialStage: false,
                                    ),
                                  )),
                        );
                      }),
                  Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: Consumer<ProfileModel>(
                      builder: (context, profileModel, child) {
                        return ListTile(
                            leading: SvgPicture.asset(
                              'assets/images/browser.svg',
                              width: 40,
                              height: 40,
                              fit: BoxFit.fill,
                            ),
                            title: const Text('Default Browser'),
                            subtitle:
                                const Text('Click to change default browser'),
                            trailing: Switch.adaptive(
                                value: profileModel.isDefaultBrowserActivated,
                                onChanged: (value) {
                                  profileModel.isDefaultBrowserActivated =
                                      value;
                                  updateUserSettings(value);
                                  //_updateBrowserSettings(value);
                                }));
                      },
                    ),
                  ),
                  _sectionHeading('Arcade App'),
                  _optionItem(
                      title: 'About',
                      icon: 'assets/images/info.svg',
                      subtitle: 'About the arcade app',
                      tap: () {
                        showAlert(
                            context: context,
                            title: 'Arcade',
                            body: Row(
                              children: const [
                                Text('Made by Sandster with \u2764'),
                                //Icon(Arcade)
                              ],
                            ));
                      }),
                  _optionItem(
                      title: 'License',
                      icon: 'assets/images/license.svg',
                      subtitle: 'View licenses',
                      tap: () {}),
                ],
              ),
            )),
        OutlinedButton(
          style: OutlinedButton.styleFrom(
            side: const BorderSide(width: 2.0, color: Colors.redAccent),
          ),
          onPressed: () {
            _showConfirmation();
          },
          child: const Text(
            'Logout',
            style: TextStyle(color: Colors.redAccent),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(),
        )
      ],
    );
  }

  Widget _sectionHeading(String heading) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 15, 0),
      child: Row(
        children: [
          Text(
            heading,
            style: headingText4,
          ),
          const Expanded(child: Divider())
        ],
      ),
    );
  }

  Widget _optionItem(
      {required String title,
      required String subtitle,
      required String icon,
      required VoidCallback tap}) {
    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: ListTile(
          leading: SvgPicture.asset(
            icon,
            width: 40,
            height: 40,
            fit: BoxFit.fill,
          ),
          title: Text(title),
          subtitle: Text(subtitle),
          onTap: tap,
          trailing: const Icon(
            Icons.arrow_forward_ios,
            color: primaryColor,
          )),
    );
  }

  Future<void> _showConfirmation() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Logout'),
          content: const SingleChildScrollView(
            child: Text('Are you sure you want to logout?'),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Yes'),
              onPressed: () async {
                await logout();
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  (Route<dynamic> route) => false,
                );
              },
            ),
            TextButton(
              child: const Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}
